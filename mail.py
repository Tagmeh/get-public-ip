import email.message
import smtplib
import ssl
from configparser import ConfigParser


class Mail:
    def __init__(self, cc: str, config: ConfigParser):
        self.to = config["DEFAULT"]["email_to"]
        self._from = config["DEFAULT"]["email_from"]
        self.cc = cc
        self.subject = None
        self.smtp_server = config["DEFAULT"]["email_smtp_server"]
        self.port = config["DEFAULT"]["email_port"]
        self.password = config["DEFAULT"]["email_password"]
        self.message: email.message.Message() = None
        self.server: smtplib.SMTP = None
        self.payload: str = ""

    def build_subject(self, ip_address):
        self.subject = f"Get Public IP Script {ip_address}"

    def build_payload(self, ip_address, change_count):
        self.payload = f"""

                Current Public IP Address: {ip_address}
        
                Camera Link: {ip_address}:5001/ui3.htm
        
                Times the public IP has changed since 7/25/21: {change_count}
                """

    def setup_message(self):
        self.message = email.message.EmailMessage()
        self.message["From"] = self._from
        self.message["To"] = self.to
        self.message["Cc"] = self.cc
        self.message["Subject"] = self.subject
        self.message.add_header("Content-Type", "text")
        self.message.set_payload(self.payload)

    def setup_connection(self):
        context = ssl.create_default_context()
        self.server = smtplib.SMTP(self.smtp_server, self.port)
        self.server.ehlo()
        self.server.starttls(context=context)
        self.server.login(self.message["From"], self.password)

    def send_email(self):
        self.server.send_message(
            self.message,
            self.message["From"],
            self.message["To"],
        )
