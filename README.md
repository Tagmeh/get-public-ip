# Get Public IP

Script used to stay up to date on what public ip is set for a location where this script is installed and ran. 
Written to assist in taking care of aging grandparents.

The script pulls the local network's public IP address, compares it with a stored file of a previously gathered IP address.
If the IP address do not match, it sends an email to the email_cc_alt contacts.
The script always sends an email of the current IP address on a normal run to the email_to contact for sanity reasons.

The default_config is setup for Gmail.