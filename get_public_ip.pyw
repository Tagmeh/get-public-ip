import configparser

from mail import Mail
import local_files
import public_ip



def main():
    config = configparser.ConfigParser()
    config.read('config.ini')

    ip_address = public_ip.query()

    contents = local_files.open_ip_address_file()
    previous_ip_address, change_count = local_files.parse_contents(contents)

    cc = config['DEFAULT']['email_cc']

    if previous_ip_address != ip_address:
        cc = config['DEFAULT']['email_cc_alt']
        change_count = str(int(change_count) + 1)
        local_files.save_ip_address(ip_address, change_count)

    mail = Mail(cc=cc, config=config)

    mail.build_subject(ip_address)
    mail.build_payload(ip_address, change_count)

    mail.setup_message()
    mail.setup_connection()
    mail.send_email()


if __name__ == '__main__':
    main()
