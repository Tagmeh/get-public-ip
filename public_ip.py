import requests


def query():
    response = requests.get(r"http://api.ipify.org?format=json")
    data = response.json()
    return data.get("ip")
