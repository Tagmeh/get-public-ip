from typing import Tuple


def open_ip_address_file():
    try:
        with open(r"public_ip.txt", "r") as f:
            return f.read().split(",")
    except:
        # Intentionally broad, as this should not stop the script for any reason.
        return


def save_ip_address(ip_address: str, change_count: str):
    try:
        with open(r"public_ip.txt", "w+") as f:
            f.write(f"{ip_address},{change_count}")
    except:
        # Intentionally broad, as this should not stop the script for any reason.
        return


def parse_contents(contents) -> Tuple[str, str]:
    try:
        ip_address = contents[0]
    except TypeError:
        ip_address = "0.0.0.0"

    try:
        change_count = contents[1]
    except TypeError:
        change_count = -1

    return ip_address, change_count
